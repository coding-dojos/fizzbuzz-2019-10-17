BUZZ = "Buzz"
FIZZ = "Fizz"


class Fizzbuzz(object):
    def shout(self, number):
        result = ""
        if _is_multiple_or_contains(number, 3):
            result += FIZZ
        if _is_multiple_or_contains(number, 5):
            result += BUZZ
        if result:
            return result
        return str(number)


def _is_multiple(number: int, multiple: int) -> bool:
    return number % multiple == 0


def _contain(number: int, check: int) -> bool:
    return str(check) in str(number)


def _is_multiple_or_contains(number: int, check: int) -> bool:
    return _is_multiple(number, check) or _contain(number, check)
