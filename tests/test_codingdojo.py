import pytest

from codingdojo import __version__
from codingdojo.fizzbuzz import Fizzbuzz


@pytest.fixture()
def fizzbuzz_fixture():
    return Fizzbuzz()


def test_version():
    assert __version__ == '0.1.0'


@pytest.mark.parametrize("number,expected", [
    (2, "2"),
    (4, "4"),
    (3, "Fizz"),
    (6, "Fizz"),
    (5, "Buzz"),
    (10, "Buzz"),
    (15, "FizzBuzz"),
    (30, "FizzBuzz"),
    (13, "Fizz"),
    (23, "Fizz"),
    (52, "Buzz")
])
def test_fizz_not_fuzz(number, expected, fizzbuzz_fixture):
    """
    Given : 6
    When : Fizzbuzz().shout called with 6
    Then : return fizz
    """
    # action
    result: str = fizzbuzz_fixture.shout(number)

    # assert
    assert result == expected


